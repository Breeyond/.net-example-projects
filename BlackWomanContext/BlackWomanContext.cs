﻿using System.Data.Entity;
using Entity_Review.Classes;



namespace Entity_Review.Classes
{
    public class BlackWomanContext : DbContext
    {
        //DB sets are repositories that maintain our domain classes in-memory and are responsible for all database interactions/changetracking
        public DbSet<BlackWoman> BlackWoman { get; set; }
        public DbSet<HairCareProduct> HairCareProducts { get; set; }

        public DbSet<HairType> HairTypes { get; set; }
        //DbSets are the "organizational"  unit that maintains types (states of each class? [updated, deleted, created?]) in-memory. Queries are performed through them
    }
}
