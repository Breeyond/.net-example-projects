﻿using System.Collections.Generic;

namespace Entity_Review.Classes
{

    // here are the domain classes for our project. Black women don't share hair products (though I'm sure they would). After creating these, we added a data model project to configure the data context for our classes - this included installing the Entity Framework API
    public class BlackWoman : Entity
    {
        public string Name { get; set; }
        public bool? EntertainsFoolery { get; set; }

        public HairType HairType { get; set; }
        public int HairTypeId { get; set; }

        public List<HairCareProduct> ProductsOwned { get; set; }
    }

    public class HairType : Entity 
    {
        public string Texture { get; set; }
        public List<BlackWoman> BlackWomyn { get; set; }
    }

    public class HairCareProduct : Entity
    {
        public string Name { get; set; }
        public ProductType Type {get; set; }
        public BlackWoman owner { get; set; }
    }

    public enum ProductType
    {
        CoconutOil,
        BlackJamaicanCastorOil,
        RawSheaButter,
        SatinBonnet,
        HairLotion
    }

    public class Entity
    {
        public int Id { get; set; }
    }
}
